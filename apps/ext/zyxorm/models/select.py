#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     select
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/8
-------------------------------------------------
   修改描述-2021/7/8:         
-------------------------------------------------
"""
from dataclasses import dataclass
from ..models.base import Base
from ..exceptions import ClientConfigBadException
from typing import List, Union, Tuple


@dataclass
class Select(Base):
    pass

    # 要刷选的字段信息，默认是全部字段都刷新

    def select(self, *args):
        '''
        支持字符串，元组和列表形式的参数传入
        :param args:
        :return:
        '''
        if len(args) == 1 and isinstance(args[0], str):
            # 如果是字符串类型，且带有，进行切分
            if ',' in args[0]:
                for v in args[0].split(','):
                    self.__select_fields__.append(v.strip())
            else:
                # 单纯的一个字符串没有，的直接添加
                self.__select_fields__.append(args[0])
        elif len(args) == 1 and isinstance(args[0], List):
            for v in args[0]:
                self.__select_fields__.append(v.strip() if isinstance(v, str) else v)
        elif len(args) > 1 and isinstance(args, Tuple):
            for v in args:
                self.__select_fields__.append(v.strip() if isinstance(v, str) else v)
        print(self.__select_fields__)
        return self

    def do_select(self, isback___sql__=False):
        if not self.select_table:
            raise ClientConfigBadException(errmsg='请设置需要查询查询的表')
        if not self.__select_fields__:
            self.__select_fields__.append('*')

        # 子SQL，如查询条件等其他的组合
        # subsql = ''.join([self._compile_where(), self._compile_whereor(), self._compile_orwhere(), self._compile_groupby(), self._compile_orderby(),self._compile_having(), self._compile_offset(), self._compile_lock()])
        subsql = ''.join([self._compile_where(), self._compile_groupby(), self._compile_orderby(), self._compile_lock(), self._compile_limit_offset()])
        joinsql = ''.join(self._compile_leftjoin())

        if self.__returning__:
            raise ClientConfigBadException(errmsg='select查询中不允许调用returning函数！请检测！')

        self.__sql__ = 'select {} from {}{}{}'.format(','.join(self.__select_fields__), self.select_table, joinsql, subsql)

        # returnsql = "select {} from {}{}{}".format(self._compile_limit(), ','.join(self.__select__), self._tablename(), joinsql, subsql)
        if isback___sql__:
            return self.__sql__
        return self
