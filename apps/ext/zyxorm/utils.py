#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     utils
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/2
-------------------------------------------------
   修改描述-2021/7/2:         
-------------------------------------------------
"""
import six
import datetime
import random
import hashlib
import json


def disable_urllib3_warning():
    """
    https://urllib3.readthedocs.org/en/latest/security.html#insecurerequestwarning
    InsecurePlatformWarning 警告的临时解决方案
    """
    try:
        import urllib3
        # 禁用安全请求警告 py3
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    except Exception:
        pass


def to_text(value, encoding='utf-8'):
    """将 value 转为 unicode，默认编码 utf-8

    :param value: 待转换的值
    :param encoding: 编码
    """
    if not value:
        return ''
    if isinstance(value, six.text_type):
        return value
    if isinstance(value, six.binary_type):
        return value.decode(encoding)
    return six.text_type(value)


def to_binary(value, encoding='utf-8'):
    """将 values 转为 bytes，默认编码 utf-8

    :param value: 待转换的值
    :param encoding: 编码
    """
    if not value:
        return b''
    if isinstance(value, six.binary_type):
        return value
    if isinstance(value, six.text_type):
        return value.encode(encoding)

    if six.PY3:
        return six.binary_type(str(value), encoding)  # For Python 3
    return six.binary_type(value)


def get_datetime():
    """获取年月日时分秒毫秒共10位数的字符串"""
    return datetime.datetime.now().strftime("%Y%m%d%H%M%S")


def generate_code_len(len=4):
    # 定义一个种子，从这里面随机拿出一个值，可以是字母
    seeds = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    # 定义一个空列表，每次循环，将拿到的值，加入列表
    random_num = []
    # choice函数：每次从seeds拿一个值，加入列表
    random_str = [random.choice(seeds) for i in range(len)]
    # 将列表里的值，变成四位字符串
    return "".join(random_str)


def md5(text):
    """md5加密函数"""
    md5 = hashlib.md5()
    if not isinstance(text, bytes):
        text = str(text).encode('utf-8')
    md5.update(text)
    return md5.hexdigest()


def json_2_str(data):
    """
    字典数据转化为字符串
    """
    return json.dumps(data, ensure_ascii=False)


def datatime_to_gmt_string():
    from datetime import datetime
    GMT_FORMAT = '%a, %d %b %Y %H:%M:%S GMT+0800 (CST)'
    return datetime.utcnow().strftime(GMT_FORMAT)
