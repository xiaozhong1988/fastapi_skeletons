#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     memcached
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/7/28
-------------------------------------------------
   修改描述-2021/7/28:         
-------------------------------------------------
"""
from typing import Tuple

from aiomcache import Client

from apps.ext.cache.backends import Backend


class MemcachedBackend(Backend):
    def __init__(self, mcache: Client):
        self.mcache = mcache

    async def get_with_ttl(self, key: str) -> Tuple[int, str]:
        return 3600, await self.mcache.get(key.encode())

    async def get(self, key: str):
        return await self.mcache.get(key, key.encode())

    async def set(self, key: str, value: str, expire: int = None):
        return await self.mcache.set(key.encode(), value.encode(), exptime=expire or 0)

    async def clear(self, namespace: str = None, key: str = None):
        raise NotImplementedError