#!/usr/bin/evn python
# coding=utf-8

import random

import uuid
# from .encrypt_helper import md5


### 定义常量 ###
# 小写字母
lowercase_letters = "abcdefghijklmnopqrstuvwxyz"
# 大写字母
majuscule = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
# 数字
numbers = "0123456789"


################

def ___get_randoms(length, text):
    """
    内部函数，获取指定长度的随机字符
    :param length: 将要生成的字符长度
    :param text: 生成随机字符的字符池
    :return: 生成好的随机字符串
    """
    return random.sample(text, length)


def generate_code():
    # 定义一个种子，从这里面随机拿出一个值，可以是字母
    seeds = "1234567890"
    # 定义一个空列表，每次循环，将拿到的值，加入列表
    random_num = []
    # choice函数：每次从seeds拿一个值，加入列表
    random_str = [random.choice(seeds) for i in range(4)]
    # 将列表里的值，变成四位字符串
    return "".join(random_str)

def generate_code_len(len=4):
    # 定义一个种子，从这里面随机拿出一个值，可以是字母
    seeds = "1234567890"
    # 定义一个空列表，每次循环，将拿到的值，加入列表
    random_num = []
    # choice函数：每次从seeds拿一个值，加入列表
    random_str = [random.choice(seeds) for i in range(len)]
    # 将列表里的值，变成四位字符串
    return "".join(random_str)

def generate_code_len_zimu(len=4):
    # 定义一个种子，从这里面随机拿出一个值，可以是字母
    seeds = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    # 定义一个空列表，每次循环，将拿到的值，加入列表
    random_num = []
    # choice函数：每次从seeds拿一个值，加入列表
    random_str = [random.choice(seeds) for i in range(len)]
    # 将列表里的值，变成四位字符串
    return "".join(random_str)

def get_number(length):
    """
    获取指定长度的数字，类型是字符串
    :param length: 将要生成的字符长度
    :return: 生成好的随机字符串
    """
    return ''.join(___get_randoms(length, numbers))


def get_number_for_range(small, max_num):
    """
    获取指定大小的整形数值
    :param small: 最小数值
    :param max_num: 最大数值
    :return: 生成好的随机数值
    """
    return random.randint(small, max_num)


def get_string(length):
    """
    获取指定长度的字符串（大小写英文字母+数字）
    :param length: 将要生成的字符长度
    :return: 生成好的随机字符串
    """
    return ''.join(___get_randoms(length, lowercase_letters + majuscule + numbers))



def get_string_upper(length):
    """
    获取指定长度的字符串（大小写英文字母+数字）
    :param length: 将要生成的字符长度
    :return: 生成好的随机字符串
    """
    return ''.join(___get_randoms(length, lowercase_letters + majuscule + numbers)).upper()

def get_letters(length):
    """
    生成随机英文字母字符串（大小写英文字母）
    :param length: 将要生成的字符长度
    :return: 生成好的随机字符串
    """
    return ''.join(___get_randoms(length, lowercase_letters + majuscule))


def get_uuid():
    """
    随机生成uuid
    :return: 生成好的uuid
    """
    return str(uuid.uuid4()).replace('-', '')


def get_ran_string():
    """
    生成32位随机字符串
    :return: 生成好的随机字符串
    """
    from .encrypt_helper import md5
    return md5(get_uuid()).upper()

# 随机生成混合密码
def get_ran_string_codekey(s_int=7,n_int =15):
    """
   随机生成混合密码
    :return: 生成好的随机字符串
    """
    # 绝定这个密码的长度
    length = random.randint(s_int, n_int)
    # 创建一个名为random_letters的变量声明它的类型为字符串（如果不这么写后面会报错）
    random_letters = str()
    # 重置循环变量i
    i = 0
    digital = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    letter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for i in range(length):
        # 绝定这位密码的来源
        From = random.randint(1, 2)
        if From == 1:
            # 随机从列表digital中抽取一个值添加到random_letters的最后
            random_letters = random_letters + random.choice(digital)
        if From == 2:
            # 随机从列表letter中抽取一个值添加到random_letters的最后
            random_letters = random_letters + random.choice(letter)




if __name__ == '__main__':
    print(get_string(10))