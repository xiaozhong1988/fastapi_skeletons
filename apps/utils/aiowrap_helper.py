#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     aiowrap
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/6/9
-------------------------------------------------
   修改描述-2021/6/9:         
-------------------------------------------------
"""
import functools
import time


class aiwrap:
    def __init__(self, obj):
        self._it = iter(obj)

    def __aiter__(self):
        return self

    async def __anext__(self):
        try:
            value = next(self._it)
        except StopIteration:
            raise StopAsyncIteration
        return value


def time_cost(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        st = time.time()
        res = func(*args, **kwargs)
        print(f"{func.__name__} cost {round(time.time() - st, 3)}s")
        return res

    return wrapper
