#!/usr/bin/evn python
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   文件名称 :     paycallback
   文件功能描述 :   功能描述
   创建人 :       小钟同学
   创建时间 :          2021/9/28
-------------------------------------------------
   修改描述-2021/9/28:         
-------------------------------------------------
"""

from apps.modules.hanxuan.api import bp as lantu
from apps.ext.redis.syncredis2 import sync_redis_client
from apps.modules.hanxuan.schemas import Depends, WxCodeForm
from apps.config.wxpay_conf import wxpayconf
from apps.response.json_response import Fail, Success
import requests
from apps.modules.hanxuan.service import LogicService


# orE7I573pQKfy1oRZywqB1mXHzxc

@lantu.get("/wx/oauth2/access_token", summary="微信获取授权码-【小钟】")
def wx_oauth2_access_token(forms: WxCodeForm = Depends(WxCodeForm)):
    APPID = wxpayconf.GZX_ID
    SECRET = wxpayconf.GZX_SECRET
    CODE = forms.code.strip()
    resutl = requests.get(url='https://api.weixin.qq.com/sns/oauth2/access_token?appid={0}&secret={1}&code={2}&grant_type=authorization_code'.format(APPID, SECRET, CODE))
    try:
        resutl_json = resutl.json()
        if resutl.status_code == 200 and resutl_json:
            if 'errcode' in resutl_json:
                pass
                return Fail(api_code=200, result=None, message='授权处理失败,原因：{}'.format(resutl_json.get('errmsg', '未知错误')))
    except:
        return Fail(api_code=200, result=None, message='授权处理失败微信系统接口异常,原因：未知错误')

    if 'access_token' in resutl_json:
        data = {
            "openid": resutl_json.get('openid')
        }

        LogicService.creat_visit_user_info(
            openid=resutl_json.get('openid'),
            statue=1,
            uage=12,
            unicename='45345',
            uphone='45345',
            usex='男'
        )

        return Success(api_code=200, result=data, message='获取成功')
    return Fail(api_code=200, result=None, message='授权处理失败微信系统接口异常,原因：未知错误')
